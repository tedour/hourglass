/** Websocket **/
var connection = new WebSocket('ws://' + location.hostname + ':80/ws', ['arduino']);
connection.onopen = function () {
  connection.send('Connect ' + new Date());
};
connection.onerror = function (error) {
  console.log('WebSocket Error ', error);
};
connection.onmessage = function (e) {
  console.log('Server: ', e.data);
};
connection.onclose = function () {
  console.log('WebSocket connection closed');
};
/** End Websocket **/


function loadJSON(callback) {

  var xobj = new XMLHttpRequest();
      xobj.overrideMimeType("application/json");
  xobj.open('GET', 'banana.json', true); // Replace 'my_data' with the path to your file
  xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
          // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
          callback(xobj.responseText);
        }
  };
  xobj.send(null);
}



loadJSON(function(response) {
  // Parse JSON string into object
  var configJSON = JSON.parse(response);
  configJSON.cards.forEach(function(card){
    console.log(card);
    var myCard = `
    <div class="card">
      <div class="form-horizontal">
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Order</label>
          </div>
          <div class="col-9 col-sm-12">
            <input class="form-input id" placeholder="Order" type="text" value="${card.id}">
          </div>
        </div>
        <div class="form-group">
          <div class="col-9 col-sm-12 col-ml-auto">
            <label class="form-switch">
              <input class="activate" type="checkbox" checked="${card.activate}"><i class="form-icon"></i> Activate
            </label>
          </div>
        </div>
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Title</label>
          </div>
          <div class="col-9 col-sm-12">
            <input class="form-input title" id="input-title" placeholder="Order" type="text" value="${card.title}">
          </div>
        </div>
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Type</label>
          </div>
          <div class="col-9 col-sm-12">
            <select class="form-select type">
              <option>Choose an option</option>
              <option ${card.type == "Finish hour" ? "selected" : "" }>Finish hour</option>
              <option ${card.type == "Delay" ? "selected" : "" }>Delay</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Value (Hour or minutes)</label>
          </div>
          <div class="col-9 col-sm-12">
            <input class="form-input value" id="input-title" placeholder="Order" type="text" value="${card.value}">
          </div>
        </div>
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Image</label>
          </div>
          <div class="col-9 col-sm-12">
            <select class="form-select image">
              <option>Choose an option</option>
              <option ${card.image == "coffee" ? "selected" : "" }>Coffee</option>
              <option ${card.image == "shoes" ? "selected" : "" }>Shoes</option>
              <option ${card.image == "wash" ? "selected" : "" }>Wash</option>
              <option ${card.image == "game" ? "selected" : "" }>Game</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-9 col-sm-12 col-ml-auto">
            <button class="btn btn-error btn-remove">Remove</button>
          </div>
        </div>
      </div>
    </div>
    `;
    $('.main').append(myCard);
  });

});

$(document).on('click', '.btn-remove', function(e){
  console.log("Remove !");
  $(this).parent().parent().parent().parent().remove();
});

$('#save').on('click', function(e) {
  var result = {cards:[]};
  $('.card').each(function(index){
    console.log('input %d is: %o', index, this);
    var myCard = {};
    myCard.id = $(this).find('.id').val();
    myCard.activate = $(this).find('.activate').val();
    myCard.title = $(this).find('.title').val();
    myCard.type = $(this).find('.type').val();
    myCard.value = $(this).find('.value').val();
    myCard.image = $(this).find('.image').val();
    console.log(myCard);
    result.cards.push(myCard);

  });
  console.log(JSON.stringify(result));
  //Send to server
  connection.send("test");
});

$('#add').on('click', function(e) {
    var myCard = `
    <div class="card">
      <div class="form-horizontal">
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Order</label>
          </div>
          <div class="col-9 col-sm-12">
            <input class="form-input" id="input-id" placeholder="Order" type="text" value="0">
          </div>
        </div>
        <div class="form-group">
          <div class="col-9 col-sm-12 col-ml-auto">
            <label class="form-switch">
              <input type="checkbox" "checked"}><i class="form-icon"></i> Activate
            </label>
          </div>
        </div>
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Title</label>
          </div>
          <div class="col-9 col-sm-12">
            <input class="form-input" id="input-title" placeholder="Order" type="text" value="Title">
          </div>
        </div>
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Type</label>
          </div>
          <div class="col-9 col-sm-12">
            <select class="form-select">
              <option>Choose an option</option>
              <option>Finish hour</option>
              <option>Delay</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Value (Hour or minutes)</label>
          </div>
          <div class="col-9 col-sm-12">
            <input class="form-input" id="input-title" placeholder="Order" type="text" value="08:00">
          </div>
        </div>
        <div class="form-group">
          <div class="col-3 col-sm-12">
            <label class="form-label" for="input-example-1">Image</label>
          </div>
          <div class="col-9 col-sm-12">
            <select class="form-select">
              <option>Choose an option</option>
              <option>Coffee</option>
              <option>Shoes</option>
              <option>Wash</option>
              <option>Game</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-9 col-sm-12 col-ml-auto">
            <button class="btn btn-error">Remove</button>
          </div>
        </div>
      </div>
    </div>
    `;
    $('.main').append(myCard);
});
