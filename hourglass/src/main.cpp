#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <FS.h>
#include <WiFiClient.h>
#include <TimeLib.h>
#include <NtpClientLib.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ESP8266mDNS.h>
#include <Ticker.h>
#include <ArduinoOTA.h>
#include <ArduinoJson.h>
#include <FSWebServerLib.h>
#include <Hash.h>
#include <Ticker.h>

// Screen
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

U8G2_SSD1306_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
//U8G2_SSD1306_64X48_ER_1_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);   // EastRising 0.66" OLED breakout board, Uno: A4=SDA, A5=SCL, 5V powered
// SCL => D1, SDA => D2
Ticker tSecond;

void u8g2_prepare(void) {
  u8g2.setFont(u8g2_font_micro_tr);
  u8g2.setFontRefHeightExtendedText();
  u8g2.setDrawColor(1);
  u8g2.setFontPosTop();
  u8g2.setFontDirection(0);
}

//JSON

const size_t capacity = JSON_ARRAY_SIZE(10) + JSON_OBJECT_SIZE(1) + 10*JSON_OBJECT_SIZE(6) + 190;
// Allocate a buffer to store contents of the file.
std::unique_ptr<char[]> buf(new char[capacity]);
DynamicJsonBuffer jsonBuffer(capacity);


// 7 segments * 4
#include <TM1637Display.h>

const int CLK = D6; //Set the CLK pin connection to the display
const int DIO = D5; //Set the DIO pin connection to the display
int numCounter = 0;

TM1637Display display(CLK, DIO); //set up the 4-Digit Display.

byte brightnessLevel = 0; // Brightness of TM1637 and Oled, initialize to middle (0+1)
// button
#include <ClickButton.h>
// the Button
const int buttonPin1 = D8;
ClickButton button1(buttonPin1, LOW, CLICKBTN_PULLUP);

time_t prevDisplay = 0; // when the digital clock was displayed

// Cards
int cardCurrent = 0;
int cardNumbers; // number of cards
String cardMode;
String cardValue;
int cardRemainingTime;
int cardHours;
int cardMinutes;

// Load banana.json
bool loadConfig() {
  File configFile = SPIFFS.open("/banana.json", "r");
  if (!configFile) {
    Serial.println("Failed to open config file");
    return false;
  }

  configFile.readBytes(buf.get(), capacity);

  //const char* json = "{\"cards\":[{\"id\":\"1\",\"activate\":\"on\",\"title\":\"Ecoles\",\"type\":\"Finish hour\",\"value\":\"08:10\",\"image\":\"Choose an option\"},{\"id\":\"2\",\"activate\":\"on\",\"title\":\"Petit dejeuner\",\"type\":\"Delay\",\"value\":\"30\",\"image\":\"Choose an option\"}]}";


  const char* json = buf.get();
  // Test do debug
  JsonObject& JsonRoot = jsonBuffer.parseObject(json);
  // Number of elements
  JsonArray& cards = JsonRoot["cards"];
  cardNumbers = cards.size();

  JsonObject& cards_0 = JsonRoot["cards"][0];


  String title = cards_0["title"];
  Serial.print("cards:");
  Serial.println(cards.size());
  Serial.print("/");
  Serial.println(sizeof(JsonRoot["cards"]));


  configFile.close();
  return true;
}

bool saveConfig(String myJSON) {
  Serial.println(myJSON);
  File configFile = SPIFFS.open("/banana.json", "w");
  if (!configFile) {
    Serial.println("Failed to open config file");
    return false;
  }
  configFile.print(myJSON);
  configFile.close();
  Serial.println("Finish !");
  loadConfig();
}

void loadImage(String filename) {

  if (!SPIFFS.exists(filename)) {
    Serial.println("File not found");
    return;
  }
  File imagefile = SPIFFS.open(filename, "r");
  String xbm;
  u8g2_uint_t imageWidth, imageHeight;
  uint8_t imageBits[imagefile.size()/4]; //This seems inefficient
  uint16_t pos = 0;
  const char CR = 10;
  const char comma = 44;
  while(imagefile.available()) {
    char next = imagefile.read();
    if (next == CR) {
      if (xbm.indexOf("#define") == 0) {
        if (xbm.indexOf("_width ")>0) {
          xbm.remove(0,xbm.lastIndexOf(" "));
          imageWidth = xbm.toInt();
          if (imageWidth > 64) {
            Serial.println("Image too large for screen");
            return;
          }
        }
        if (xbm.indexOf("_height ")>0) {
          xbm.remove(0,xbm.lastIndexOf(" "));
          imageHeight=xbm.toInt();
          if (imageHeight > 48) {
            Serial.println("Image too large for screen");
            return;
          }
        }
      }
      xbm = "";
    } else if (next == comma) {
      imageBits[pos++] = (uint8_t) strtol(xbm.c_str(), NULL, 16);
      xbm = "";
    } else {xbm += next;}
  }
  imageBits[pos++] = (int) strtol(xbm.c_str(), NULL, 16); //turn the string into a character
  imageBits[pos]=0;
  u8g2.drawXBM(0, 0, imageWidth, imageHeight, imageBits);
}



void digitalClockDisplay(){
  // digital clock display of the time
  Serial.print(hour());
  Serial.println(minute());
  display.showNumberDecEx(hour()*100 + minute(), 64, true);
}

void digitalTimeRemainingDisplay(){
  Serial.println("digitalTimeRemainingDisplay");
  // digital clock display of the time
  if (hour() > cardHours || (hour() == cardHours && minute() > cardMinutes)) {
    // Finished
    display.showNumberDecEx(0, 64, true);
  } else {
    int result;
    if (cardMinutes >= minute()) {
      result = (cardHours - hour())*100 + (cardMinutes - minute());
    } else {
      result = (cardHours - hour() -1)*100 + (cardMinutes + 60 - minute());
    }
    Serial.println(result);
    display.showNumberDecEx(result, 64, true);
  }
}

AsyncWebSocket ws("/ws");

void onWsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
  if(type == WS_EVT_CONNECT){
    //client connected
    os_printf("ws[%s][%u] connect\n", server->url(), client->id());
    client->printf("Hello Client %u :)", client->id());
    client->ping();
  } else if(type == WS_EVT_DISCONNECT){
    //client disconnected
    os_printf("ws[%s][%u] disconnect: %u\n", server->url(), client->id());
  } else if(type == WS_EVT_ERROR){
    //error was received from the other end
    os_printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
  } else if(type == WS_EVT_PONG){
    //pong message was received (in response to a ping request maybe)
    os_printf("ws[%s][%u] pong[%u]: %s\n", server->url(), client->id(), len, (len)?(char*)data:"");
  } else if(type == WS_EVT_DATA){
    //data packet
    AwsFrameInfo * info = (AwsFrameInfo*)arg;
    if(info->final && info->index == 0 && info->len == len){
      //the whole message is in a single frame and we got all of it's data
      os_printf("ws[%s][%u] %s-message[%llu]: ", server->url(), client->id(), (info->opcode == WS_TEXT)?"text":"binary", info->len);
      if(info->opcode == WS_TEXT){
        data[len] = 0;
        os_printf("%s\n", (char*)data);
      } else {
        for(size_t i=0; i < info->len; i++){
          os_printf("%02x ", data[i]);
        }
        os_printf("\n");
      }
      if(info->opcode == WS_TEXT) {
        String inputString;
        inputString = (char*)data;
        client->text("received !");
        client->text(inputString);
        if (inputString.substring(0,5) == "JSON:") {
          // save JSON Data to banana.json
          saveConfig(inputString.substring(5));
        }
      } else {
        client->binary("I got your binary message");
      }
    } else {
      //message is comprised of multiple frames or the frame is split into multiple packets
      if(info->index == 0){
        if(info->num == 0)
          os_printf("ws[%s][%u] %s-message start\n", server->url(), client->id(), (info->message_opcode == WS_TEXT)?"text":"binary");
        os_printf("ws[%s][%u] frame[%u] start[%llu]\n", server->url(), client->id(), info->num, info->len);
      }

      os_printf("ws[%s][%u] frame[%u] %s[%llu - %llu]: ", server->url(), client->id(), info->num, (info->message_opcode == WS_TEXT)?"text":"binary", info->index, info->index + len);
      if(info->message_opcode == WS_TEXT){
        data[len] = 0;
        os_printf("%s\n", (char*)data);
      } else {
        for(size_t i=0; i < len; i++){
          os_printf("%02x ", data[i]);
        }
        os_printf("\n");
      }

      if((info->index + len) == info->len){
        os_printf("ws[%s][%u] frame[%u] end[%llu]\n", server->url(), client->id(), info->num, info->len);
        if(info->final){
          os_printf("ws[%s][%u] %s-message end\n", server->url(), client->id(), (info->message_opcode == WS_TEXT)?"text":"binary");
          if(info->message_opcode == WS_TEXT)
            client->text("I got your text message2");
          else
            client->binary("I got your binary message");
        }
      }
    }
  }
}

void oledShowImage (String xbmFile) {
  // Graphic
  u8g2.firstPage();
  do {
    /* all graphics commands have to appear within the loop body. */
    u8g2_prepare();
    loadImage(xbmFile);
  } while ( u8g2.nextPage() );
}

void showCard(int id) {
  File configFile = SPIFFS.open("/banana.json", "r");
  if (!configFile) {
    Serial.println("Failed to open config file");
  }

  configFile.readBytes(buf.get(), capacity);

  //const char* json = "{\"cards\":[{\"id\":\"1\",\"activate\":\"on\",\"title\":\"Ecoles\",\"type\":\"Finish hour\",\"value\":\"08:10\",\"image\":\"Choose an option\"},{\"id\":\"2\",\"activate\":\"on\",\"title\":\"Petit dejeuner\",\"type\":\"Delay\",\"value\":\"30\",\"image\":\"Choose an option\"}]}";


  const char* json = buf.get();

  JsonObject& JsonRoot = jsonBuffer.parseObject(buf.get());
  JsonObject& card = JsonRoot["cards"][id];
  String type = card["type"];
  String image = card["image"];
  String title = card["title"];
  String filename = "/";
  filename.concat(image);
  filename.concat(".xbm");
  Serial.println(filename);
  if (type == "Time") {
    Serial.println(title);
    oledShowImage(filename);
    cardMode = "Time";
  }

  if (type == "Finish hour") {
    Serial.println(title);
    oledShowImage(filename);
    cardMode = "Finish hour";
    String value = card["value"];
    Serial.print("value:");
    Serial.println(value);
    cardHours = value.substring(0,2).toInt();
    cardMinutes = value.substring(3,5).toInt();

  }

  if (type == "Delay") {
    Serial.println(title);
    String value = card["value"];
    Serial.print("value:");
    Serial.println(value);

    cardRemainingTime = value.toInt() * 60;
    oledShowImage(filename);
    cardMode = "Delay";
  }
  /*
  Serial.println("Show card");
  Serial.println(type);*/
}
void refresh() {
  // Time

  if (cardMode == "Delay") {
    if (cardRemainingTime > 0) {
      cardRemainingTime--;
      int minutes = cardRemainingTime / 60;
      int seconds = cardRemainingTime - minutes * 60;
      display.showNumberDecEx(minutes*100 + seconds, 64, true);
    }
  }
}

void doubleClick() {
  // Change brightness 3 levels
  brightnessLevel ++;
  if (brightnessLevel == 3) brightnessLevel = 0; // Only 0,1,2 levels
  if (brightnessLevel == 0) {
    display.setBrightness(0);
    u8g2.setContrast(5);
  }
  if (brightnessLevel == 1) {
    display.setBrightness(2);
    u8g2.setContrast(128);
  }
  if (brightnessLevel == 2) {
    display.setBrightness(7);
    u8g2.setContrast(255);
  }
}

// https://stackoverflow.com/questions/9072320/split-string-into-string-array
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}


void tripleClick() {
  // Show ip address
  u8g2.clear();

  do {
    /* all graphics commands have to appear within the loop body. */
    // Graphic
    u8g2.setCursor(0, 0);
    u8g2.print(WiFi.localIP());
    u8g2.setCursor(0, 15);
    u8g2.print(WiFi.SSID());

    //u8g2.drawXBM( 0, 0, coffee_width, coffee_height, coffee_bits);
  } while ( u8g2.nextPage() );

  cardCurrent --; // Show same card on next single click
}

void setup() {
    // WiFi is started inside library
    SPIFFS.begin(); // Not really needed, checked inside library and started if needed

    /* add setup code here */
    Serial.begin(115200);
    Serial.println("Load");
    u8g2.begin();
    u8g2_prepare();
    u8g2.firstPage();

    do {
      /* all graphics commands have to appear within the loop body. */
      loadImage("/hourglass.xbm");

      //u8g2.drawXBM( 0, 0, coffee_width, coffee_height, coffee_bits);
    } while ( u8g2.nextPage() );


    display.setBrightness(0x0a); //set the diplay to maximum brightness
    doubleClick(); // Initialize brightness
    display.showNumberDecEx(0, 64, true);
    // Setup button timers (all in milliseconds / ms)
    // (These are default if not set, but changeable for convenience)
    button1.debounceTime   = 20;   // Debounce timer in ms
    button1.multiclickTime = 300;  // Time limit for multi clicks
    button1.longClickTime  = 500; // time until "held-down clicks" register

    ESPHTTPServer.begin(&SPIFFS);
    ESPHTTPServer.on("/heap", HTTP_GET, [](AsyncWebServerRequest *request){
      int args = request->args();
      for(int i=0;i<args;i++){
        Serial.printf("ARG[%s]: %s\n", request->argName(i).c_str(), request->arg(i).c_str());
      }
      request->send(200, "text/plain", "hello !");
    });
    ESPHTTPServer.on("/command", HTTP_GET, [](AsyncWebServerRequest *request){
      // Send a command via http : http://192.168.1.140/command?command=SP%20100
      int args = request->args();
      String inputString;
      for(int i=0;i<args;i++){

        Serial.printf("ARG[%s]: %s\n", request->argName(i).c_str(), request->arg(i).c_str());
        //if (request->argName(i).c_str() == "command") {
          inputString = request->arg(i).c_str();
        //}
      }
      Serial.println(inputString);

      request->send(200, "text/plain", "command sended !");
    });


    ws.onEvent(onWsEvent);
    ESPHTTPServer.addHandler(&ws);

    loadConfig();

    // Graphic
    u8g2.firstPage();
    do {
      /* all graphics commands have to appear within the loop body. */
      u8g2_prepare();
      loadImage("/game.xbm");
    } while ( u8g2.nextPage() );
    //showCard(cardCurrent);
    tSecond.attach(1, refresh);
}


void loop() {
    /* add main program code here */

    // Update button state
    button1.Update();

    // Save click codes in LEDfunction, as click codes are reset at next Update()
    if (button1.clicks != 0) {
      Serial.println(button1.clicks);
      if (button1.clicks == -1) { // 1 click => Next card
        Serial.println(cardCurrent);
        cardCurrent ++;
        if (cardCurrent == cardNumbers) cardCurrent = 0;
        showCard(cardCurrent);
      }
      if (button1.clicks == -2) { // double click
        doubleClick();
      }
      if (button1.clicks == -3) { // triple click
        tripleClick();
      }

    }


    if (timeStatus() != timeNotSet) {
      if (now() != prevDisplay) { //update the display only if time has changed
        prevDisplay = now();
        if (cardMode == "Time")
          digitalClockDisplay();
        if (cardMode == "Finish hour")
          digitalTimeRemainingDisplay();

      }
    }


    // DO NOT REMOVE. Attend OTA update from Arduino IDE
    ESPHTTPServer.handle();
}
