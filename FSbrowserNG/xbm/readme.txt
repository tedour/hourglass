* Load a clipart (openclipart)
* Open in Gimp
* Resize (Image - Echelle et taille de l'image)
* Largeur max : 64, hauteur max : 48
* Taille du canevas (To get the right size)
* Netetée : Calque -  Transparence - Seuil Alpha
* Exporter (Fichier - Exporter - fichier xbm)
