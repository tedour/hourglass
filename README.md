The main code is in the hourglass folder.

## TEST

Other folders are for testing purpose :

* FSBrowserNG : Base firmware for esp8266 with lot of features
* TM1637 : Test TM1637 7-segment display module
* ssd1306 : Test ssd screen 
* u8G2 and oled-nano : Test libs for ssd screen

## Wire

hourglass.fzz is connection diagram for wires, open it with http://fritzing.org/home/[Fritzing]

## Installation

You must install multiple libraries for compiling this project, if you get error, please report in issues. It's easier to install Atom + Platformio.

* Install https://atom.io/[atom.io]

* From atom, install **platformio-ide** package

* From Platformio, install Platform **Espressif 8266**

* Select the hourglass directory

* Upload the code (Menu PlatformIO - Upload)

* Upload data (Menu PlatformIO - Run other target / Choose PIO Upload File System image)


## Programming 

* Main program : src/main.cpp

* Interface : data/index.htm, data/banana.js
